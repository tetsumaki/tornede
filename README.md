Tornede (unmaintained 2013/06/16)
=======

- Tornede est un thème pour le logiciel Leed.
- Il utilise les standarts HTML5 et CSS3 ainsi que la bibliothèque jQuery.
- L'interface est à la base inspiré par Google Reader et Feedly.
- Le thème a été testé sous Chromium@Linux, Firefox@Linux, Chrome@Android, Firefox@Android, IE10@Windows.
- Il est donc en théorie compatible avec tous les navigateurs utilisant le moteur de rendu WebKit ainsi que Gecko.
- Il n'est pas compatible avec IE8 et les versions antérieures.

Leed
====

- Leed (contraction de Light Feed) est un agrégateur RSS/ATOM minimaliste qui permet la consultation de flux RSS de manière rapide et non intrusive.
- Site : <http://projet.idleman.fr/leed>

Liens
=====

- W3C : <http://validator.w3.org/>
- W3C : <http://jigsaw.w3.org/css-validator/>
