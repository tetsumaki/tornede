function toggleFolder(element,folder){
	feedBloc = $(element).closest('.nav-feeds').find('.nav-feed').parent();

	open = 0;
	if(feedBloc.css('display')=='none') open = 1;
	feedBloc.toggle();
	$(element).html((!open?'+':'-'));
	$.ajax({
				  url: "./action.php?action=changeFolderState",
				  data:{id:folder,isopen:open}
	});
}

function addFavorite(element,id){
	$(element).attr('onclick','removeFavorite(this,'+id+');').removeClass('button-artfav-isnot').addClass('button-artfav-is');
	$.ajax({
				  url: "./action.php?action=addFavorite",
				  data:{id:id}
	});
}

function removeFavorite(element,id){
	$(element).attr('onclick','addFavorite(this,'+id+');').removeClass('button-artfav-is').addClass('button-artfav-isnot');
	$.ajax({
				  url: "./action.php?action=removeFavorite",
				  data:{id:id}
	});
}

function addRead(element,id){
	$(element).attr('onclick','removeRead(this,'+id+');').removeClass('button-artunread-is').addClass('button-artunread-isnot');
	$.ajax({
				  url: "./action.php?action=unreadContent",
				  data:{id:id}
	});
}

function removeRead(element,id){
	$(element).attr('onclick','addRead(this,'+id+');').removeClass('button-artunread-isnot').addClass('button-artunread-is');
	$.ajax({
				  url: "./action.php?action=readContent",
				  data:{id:id}
	});
}

function renameFeed(element,feed){
	var feedLine = $(element).parent().parent();
	var feedNameCase = $('li:first a',feedLine);
	var feedNameValue = feedNameCase.html();
	var feedUrlCase = $('li:first h3',feedLine);
	var feedUrlValue = feedUrlCase.html();
	var url = feedNameCase.attr('href');
	$(element).html('Enregistrer');
	$(element).attr('onclick','saveRenameFeed(this,'+feed+',"'+url+'")');
	feedNameCase.replaceWith('<input type="text" name="feedName" value="'+feedNameValue+'" />');
	feedUrlCase.replaceWith('<input type="text" name="feedUrl" value="'+feedUrlValue+'" />');
}

function saveRenameFeed(element,feed,url){
	var feedLine = $(element).parent().parent();
	var feedNameCase = $('li:first input[name="feedName"]',feedLine);
	var feedNameValue = feedNameCase.val();
	var feedUrlCase = $('li:first input[name="feedUrl"]',feedLine);
	var feedUrlValue = feedUrlCase.val();
	$(element).html('Renommer');
	$(element).attr('onclick','renameFeed(this,'+feed+')');
	feedNameCase.replaceWith('<a href="'+url+'">'+feedNameValue+'</a>');
	feedUrlCase.replaceWith('<h3>'+feedUrlValue+'</h3>');
	$.ajax({
				  url: "./action.php?action=renameFeed",
				  data:{id:feed,name:feedNameValue,url:feedUrlValue}
	});
}

function renameFolder(element,folder){
	var folderLine = $(element).parent().parent();
	var folderNameCase = $('legend:first span',folderLine);
	var value = folderNameCase.html();
	$(element).html('Enregistrer');
	$(element).attr('onclick','saveRenameFolder(this,'+folder+')');
	folderNameCase.replaceWith('<input type="text" name="folderName" value="'+value+'" />');
}


function saveRenameFolder(element,folder){
	var folderLine = $(element).parent().parent();
	var folderNameCase = $('legend:first input[name="folderName"]',folderLine);
	var value = folderNameCase.val();
	$(element).html('Renommer');
	$(element).attr('onclick','renameFolder(this,'+folder+')');
	folderNameCase.replaceWith('<span>'+value+'</span>');
	$.ajax({
				  url: "./action.php?action=renameFolder",
				  data:{id:folder,name:value}
	});
}

function changeFeedFolder(element,id){
	var value = $(element).val();
	window.location = "./action.php?action=changeFeedFolder&feed="+id+"&folder="+value;
}

function synchronize(code){
	if(code!=''){
	$('aside#aside-right').html('<aside id="aside-right">'+
	'<iframe class="importFrame" src="action.php?action=synchronize&format=html&code='+code+'" name="idFrameSynchro" id="idFrameSynchro" style="width: 100%; height:400px; border:0;" ></iframe>'+
	'</aside>');
	}else{
		alert('Vous devez être connecté pour synchroniser vos flux');
	}
}