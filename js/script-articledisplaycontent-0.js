$(document).ready( function () {
	$(".title-article").click( function () {
		var id = $(this).closest('.nav-infos').find('.anchor').attr('id');
		var button = $(this).closest('.nav-infos').find('.button-artunread-isnot');

		$(button).attr('onclick','addRead(this,'+id+');').removeClass('button-artunread-isnot').addClass('button-artunread-is');
		$.ajax({
			url: "./action.php?action=readContent",
			data:{id:id}
		});
	});
});
