$(document).ready( function () {
	$(".article-content").hide();
	$(".article-info-bottom").hide();
	$(".title-article").click( function () {
		var obj = $(this).closest('.nav-infos').find('.article-content');
		var obj2 = $(this).closest('.nav-infos').find('.article-info-bottom');
		var obj3 = $(this).parent();
		var id = $(this).closest('.nav-infos').find('.anchor').attr('id');
		var button = $(this).closest('.nav-infos').find('.button-artunread-isnot');

		if (obj.css('display') == 'none') var show = 1;
		else show = 0;

		$('.article-content').hide();
		$('.article-info-bottom').hide();
		$('.article-info-top').removeClass('article-info-top-isselected');

		if (show) {
			obj.show();
			obj2.show();
			obj3.addClass('article-info-top-isselected');
			
			window.location = '#'+id;

			$(button).attr('onclick','addRead(this,'+id+');').removeClass('button-artunread-isnot').addClass('button-artunread-is');
			$.ajax({
				url: "./action.php?action=readContent",
				data:{id:id}
			});
		}
		return false;
	});
});
